## 前言

为了更切合生产环境，这里使用win10上自带的虚拟化Hyper-V安装Centos7。

## 准备工作

### 镜像下载

版本：CentOS-7-x86_64-Minimal-2003
下载地址：http://mirrors.aliyun.com/centos/7.8.2003/isos/x86_64/CentOS-7-x86_64-Minimal-2003.iso

### 网段设置

可以统一一下虚拟机的网段

网络->属性->更改适配器设置

![image-20200917113207185](http://qiniu.mldong.com/mldong-article/images/image-20200917113207185.png)

右键->属性->Internet协议版本4（TCP/IPv4）->双击->高级设置->添加

![image-20200917113435525](http://qiniu.mldong.com/mldong-article/images/image-20200917113435525.png)

记住这里的IP地址`192.168.1.1`，一会儿虚拟机中配置需要。

## 开始安装

打开Hyper-V管理器

![image-20200917113904717](http://qiniu.mldong.com/mldong-article/images/image-20200917113904717.png)

右键->新建->虚拟机

![image-20200917114057393](http://qiniu.mldong.com/mldong-article/images/image-20200917114057393.png)

修改名称，选择存储位置，下一步

![image-20200917114233584](http://qiniu.mldong.com/mldong-article/images/image-20200917114233584.png)

默认第一代，下一步

![image-20200917114340885](http://qiniu.mldong.com/mldong-article/images/image-20200917114340885.png)

分配内存，动态内存，下一步

![image-20200917114434515](http://qiniu.mldong.com/mldong-article/images/image-20200917114434515.png)

配置网络，选择默认交换机，下一步

![image-20200917114642443](http://qiniu.mldong.com/mldong-article/images/image-20200917114642443.png)

虚拟硬盘配置,大小一般20G-40G即可，根据需要调整，下一步

![image-20200917114749661](http://qiniu.mldong.com/mldong-article/images/image-20200917114749661.png)

选择上面下载好的Centos7镜像

![image-20200917114939815](http://qiniu.mldong.com/mldong-article/images/image-20200917114939815.png)

完成

![image-20200917115019894](http://qiniu.mldong.com/mldong-article/images/image-20200917115019894.png)

右键启动，右键连接

![image-20200917115148664](http://qiniu.mldong.com/mldong-article/images/image-20200917115148664.png)

选择Install CentOS7 

![image-20200917115311532](http://qiniu.mldong.com/mldong-article/images/image-20200917115311532.png)

选择中文->简体中文(中国)->继续![image-20200917115441380](http://qiniu.mldong.com/mldong-article/images/image-20200917115441380.png)

设置安装位置

![image-20200917115539666](http://qiniu.mldong.com/mldong-article/images/image-20200917115539666.png)

勾选本地标准磁盘，完成

![image-20200917120050552](http://qiniu.mldong.com/mldong-article/images/image-20200917120050552.png)

主机名根据情况修改->应用，打开网络，完成

![image-20200917151234761](http://qiniu.mldong.com/mldong-article/images/image-20200917151234761.png)

开始安装

![image-20200917151822649](http://qiniu.mldong.com/mldong-article/images/image-20200917151822649.png)

设置密码,耐心等待安装

![image-20200917151859399](http://qiniu.mldong.com/mldong-article/images/image-20200917151859399.png)

![image-20200917152053680](http://qiniu.mldong.com/mldong-article/images/image-20200917152053680.png)

重启

![image-20200917160224238](http://qiniu.mldong.com/mldong-article/images/image-20200917160224238.png)

修改为静态IP,上述设置的网段:`192.168.1.1`

``` shell
vi /etc/sysconfig/network-scripts/ifcfg-eth0
```

修改前：

![image-20200917160623869](http://qiniu.mldong.com/mldong-article/images/image-20200917160623869.png)

修改后:

![image-20200917160926165](http://qiniu.mldong.com/mldong-article/images/image-20200917160926165.png)

``` shell
# 要设置的IP
IPADDR=192.168.1.100
# 网关，即准备工作中的适配器设置的IProroot

GATEWAY=192.168.1.1
# 子网掩码，即准备工作中的适配器设置的子网掩码
NETMASK=255.255.255.0
# DNS,与网关一致
DNS1=192.168.1.1
```

保存，重启网络

``` shell
systemctl restart network
```

查看当前IP

``` shell
ip addr
```

看能否上网

``` shell
ping www.baidu.com
```

网络正常，配置成功！

## SSH连接工具

Putty/Xshell/FinalShell/SecureCRT等

大家根据个人喜好，选一个连接工具。