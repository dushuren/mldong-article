## 前言

每次重装系统，开发环境都需要重新安装一次，着实不方便，这里介绍一种更为方便的安装方式-docker。本来想使用win10系统下的子系统的，后面发现子系统上安装docker并不是很稳定，所以这里采用的是Hyper-V上安装虚拟机的方式安装Docker。

![image-20200808101522091](http://qiniu.mldong.com/mldong-article/images/image-20200808101522091.png)

## 基础环境

![image-20200808101312138](http://qiniu.mldong.com/mldong-article/images/image-20200808101312138.png)

### 启用Linux子系统

1. 先进入控制面板->程序->启用或关闭Windows功能

   ![image-20200808103606086](http://qiniu.mldong.com/mldong-article/images/image-20200808103606086.png)

2. 去应用商店下载Ubuntu，任意版本

   建议下载18.04版本。太新的版本，docker支持不是很好。

   ![image-20200808104130366](http://qiniu.mldong.com/mldong-article/images/image-20200808104130366.png)

   ![image-20200808104040969](http://qiniu.mldong.com/mldong-article/images/image-20200808104040969.png)

   ![image-20200808130401776](http://qiniu.mldong.com/mldong-article/images/image-20200808130401776.png)

3. 启用Ubuntu，需要等一会，安装

   ![image-20200808105019141](http://qiniu.mldong.com/mldong-article/images/image-20200808105019141.png)

4. 使用管理员身份启动

   ![image-20200808131726342](http://qiniu.mldong.com/mldong-article/images/image-20200808131726342.png)

5. 设置用户名、密码，用户名不能有大写，密码输入时看不见是正常的

   ![image-20200808131843371](http://qiniu.mldong.com/mldong-article/images/image-20200808131843371.png)

6. 到此，一个纯净的Ubuntu系统安装完成。

### 更换源

默认的Ubuntu源在国外，用sudo apt install会比较慢，所以要更换成国内的源，如阿里云源等。

1. 先备份旧的源

   ``` shell
    sudo cp /etc/apt/sources.list /etc/apt/sources.list.backup
   ```

2. 修改文件

   ``` shell
   sudo bash -c "cat << EOF > /etc/apt/sources.list
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs) main restricted
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs)-updates main restricted
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs) universe
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs)-updates universe
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs) multiverse
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs)-updates multiverse
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs)-backports main restricted universe multiverse
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs)-security main restricted
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs)-security universe
   deb http://mirrors.aliyun.com/ubuntu/ $(lsb_release -cs)-security multiverse
   EOF"
   ```

   ![image-20200808131943708](http://qiniu.mldong.com/mldong-article/images/image-20200808131943708.png)

3. 保存，执行更新

   ``` shell
   sudo apt update
   sudo apt upgrade
   ```

### 安装docker

1. 安装必要的依赖

   ``` shell
   sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
   ```

2. 导入源仓库的GPG key

   ``` shell
   sudo curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
   ```

   报错了： can't connect to the agent: IPC connect call failed

   执行如下命令：

   ``` shell
   sudo apt remove gpg
   sudo apt install gnupg1
   ```

   然后继续2

3. 将Docker APT软件源添加到系统

   ``` shell
   sudo add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
   ```

4. 开始安装

   ``` shell
   sudo apt update
   sudo apt install docker-ce docker-ce-cli containerd.io
   ```

5. 然后发现，直接用不了

   ![image-20200808183453573](http://qiniu.mldong.com/mldong-article/images/image-20200808183453573.png)
   
6. 卸载换个版本重装

   ``` shell
   sudo apt purge docker-ce
   sudo apt autoremove
   ```

7. 查看可安装的docker版本

   ![image-20200808192352346](http://qiniu.mldong.com/mldong-article/images/image-20200808192352346.png)

   ![image-20200808192421969](http://qiniu.mldong.com/mldong-article/images/image-20200808192421969.png)

8. 安装`docker-ce=18.06.1~ce~3-0~ubuntu`版本

   ``` shell
   sudo apt install docker-ce=5:18.09.0~3-0~ubuntu-bionic docker-ce-cli=5:18.09.0~3-0~ubuntu containerd.io
   ```

9. 

   

